#!/usr/bin/php -q
<?php

error_reporting( 255 );
ini_set( 'display_errors', true );
date_default_timezone_set( 'Europe/Amsterdam' );

require_once('telnet.class.php');

$to   = 'test@jeroenvermeulen.eu';
$from = 'info@jeroenvermeulen.eu';

$telnet = new Telnet();

$telnet->set_host( 'mailin-fern.alphamegahosting.com' );
$telnet->set_port( 25 );
$telnet->set_timeout( 30 ); // Seconds

echo "--- Connecting.\n";
$telnet->connect();

echo "--- Waiting for '220 ...' line.\n";
$telnet->read_to( '220 ' );
$telnet->read_to( "\n" );
echo $telnet->get_buffer();

echo "--- Sending EHLO command.\n";
$telnet->write( 'EHLO test.nl' );
echo $telnet->get_buffer();

echo "--- Waiting for '250 ...' line.\n";
$telnet->read_to( '250 ' );
$telnet->read_to( "\n" );
echo $telnet->get_buffer();

echo "--- Sending MAIL FROM command.\n";
$telnet->write( 'MAIL FROM: ' . $from );

echo "--- Waiting for '250 ...' line.\n";
$telnet->read_to( '250 ' );
$telnet->read_to( "\n" );

echo "--- Sending RCPT TO command.\n";
$telnet->write( 'RCPT TO: ' . $to );

echo "--- Waiting for '250 ...' line.\n";
$telnet->read_to( '250 ' );
$telnet->read_to( "\n" );
echo $telnet->get_buffer();

echo "--- Sending DATA command.\n";
$telnet->write( 'DATA' );

echo "--- Waiting for '354 ...' line.\n";
$telnet->read_to( '354 ' );
$telnet->read_to( "\n" );

echo "--- Sending mail message.\n";
// These are the headers for the mail client software
$telnet->write( 'From: ' . $from );
$telnet->write( 'To: ' . $to );
$telnet->write( 'Subject: Test Mail' );
// Write empty line to start message body
$telnet->write( '' );
$telnet->write( 'Test executed at ' . date('r') );
$telnet->write( 'On host "' . trim( shell_exec('hostname') ) . '".' );
$telnet->write( '.' ); // A single dot on a line ends mail body

echo "--- Waiting for '250 ...' line.\n";
$telnet->read_to( '250 ' );
$telnet->read_to( "\n" );
echo $telnet->get_buffer();

echo "--- Sending QUIT.\n";
$telnet->write( 'QUIT' );

echo "--- Waiting for '221 ...' line.\n";
$telnet->read_to( '221 ' );
$telnet->read_to( "\n" );
echo $telnet->get_buffer();

echo "--- Disconnecting.\n";
$telnet->disconnect();

echo "--- Done.\n";

exit;