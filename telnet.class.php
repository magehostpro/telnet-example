<?php
/**
 * TELNET CLASS
 *
 * Originally written by Marc Ennaji (in french) - http://px.sklar.com/code.html?id=634
 * Modified by Matthias Blaser <mb@adfinis.ch> - http://adfinis.ch/
 *
 * By Phillip Kuo - http://www.phillipkuo.com/blog/?p=85
 * - translated most function- and variable names from french to english
 *
 * By Jeroen Vermeulen <info@jeroenvermeulen.eu>
 * - Fully translated
 * - Simplified
 * - Improved
 *
 */
class telnet {

    var $socket   = NULL;
    var $timeout  = 10; // Seconds
    var $host     = "";
    var $port     = "23";
    var $buffer   = "";
    var $writeEol = "\r\n";

    function connect(){
        $errno  = 0;
        $errstr = '';
        $this->socket = fsockopen( $this->host, $this->port, $errno, $errstr, $this->timeout );

        if (!$this->socket){
            throw new Exception( sprintf( "ERROR: Unable to open a telnet connection: [%d] %s", $errno, $errstr ) );
        }

        socket_set_timeout( $this->socket, $this->timeout, 0 );
    }

    function read_to( $lookFor ) {
        $NULL = chr(0);
        $IAC  = chr(255);
        $buf  = '';

        if ( ! $this->socket ){
            throw new Exception( "ERROR: Telnet socket is not open" );
        }

        while ( true ) {
            $c = fgetc( $this->socket );

            if ($c === false) {
                // No more characters to read from the socket or EOF received.
                throw new Exception( sprintf( "ERROR: Couldn't find the requested '%s' in the data returned from server: '%s'", $lookFor, $buf ) );
            }

            if ( $c == $NULL || $c == "\021" ) {
                continue; // Skip
            }

            if ($c == $IAC) {
                // Interpreted as command
                $c = fgetc( $this->socket );

                if ( $c != $IAC ) {
                    // Because the 'real' character 255 is doubled to differentiate IAC
                    $this->negotiateTelnetOption( $c );
                    continue;
                }
            }

            $buf .= $c;

            // Append current char to global buffer
            $this->buffer .= $c;

            if ( (substr($buf, strlen($buf) - strlen($lookFor) ) ) == $lookFor ) {
                // We found the expected string
                return true;
            }
        }
        return false; // Unreachable
    }

    function get_buffer() {
        return $this->buffer;
    }

    function negotiateTelnetOption( $command ){
        // Minimum options are negotiated

        $IAC  = chr(255);
        $DONT = chr(254);
        $DO   = chr(253);
        $WONT = chr(252);
        $WILL = chr(251);

        if ( ($command == $DO) || ($command == $DONT) ) {
            $opt = fgetc( $this->socket );
            fwrite( $this->socket, $IAC . $WONT . $opt );
        } else if ( ($command == $WILL) || ($command == $WONT) ) {
            $opt = fgetc( $this->socket );
            fwrite( $this->socket, $IAC . $DONT . $opt );
        } else {
            throw new Exception( sprintf( "ERROR: Unknown command %d.\n", ord($command) ) );
        }

        return true;
    }

    function write( $buffer, $addNewLine = true ){
        // Clear buffer from last command
        $this->buffer = "";

        if ( ! $this->socket ){
            throw new Exception( "ERROR: Telnet socket is not open." );
        }

        if ( $addNewLine ){
            $buffer .= $this->writeEol;
        }

        if ( fwrite($this->socket, $buffer) < 0 ){
            throw new Exception( "ERROR: Writing to socket failed." );
        }
    }

    function disconnect() {
        if ($this->socket){
            if ( ! fclose($this->socket) ){
                throw new Exception( "ERROR: Closing telnet socket failed." );
            }
            $this->socket = NULL;
        }
    }

    function set_host( $host ){
        $this->host = $host;
    }

    function set_port( $port ){
        $this->port = $port;
    }

    function set_timeout( $timeout ){
        $this->timeout = $timeout;
        if ( $this->socket ) {
            socket_set_timeout( $this->socket, $this->timeout, 0 );
        }
    }

}